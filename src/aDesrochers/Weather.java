package aDesrochers;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

public class Weather
{
    private String keyword;
    JsonElement weatherInfo;

    public Weather(String kw)
    {
        try
        {
            keyword = URLEncoder.encode(kw, "utf-8");
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
    }

    public void fetch()
    {
        String newsUrl = "https://api.aerisapi.com/observations/"
                + keyword
                + "?client_id=wQhXMMnxoRV4HNKoRLZrL"
                + "&client_secret=rUOW0GEyf5bT9JhUzro2WQAuUpj3A7nFHgVCRGEK";

        try
        {
            URL url = new URL(newsUrl);
            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is);

            weatherInfo = JsonParser.parseReader(isr);
        }
        catch (java.net.MalformedURLException mue)
        {
            System.out.println("Malformed URL");
            mue.printStackTrace();
        }
        catch (java.io.IOException ioe)
        {
            System.out.println("IO Exception");
            ioe.printStackTrace();
        }
    }

    public String getCity()
    {
        return weatherInfo.getAsJsonObject()
                .get("response").getAsJsonObject()
                .get("place").getAsJsonObject()
                .get("name").getAsString();
    }
    public String getState()
    {
        return weatherInfo.getAsJsonObject()
                .get("response").getAsJsonObject()
                .get("place").getAsJsonObject()
                .get("state").getAsString();
    }
    public String getTemperture()
    {
        return weatherInfo.getAsJsonObject()
                .get("response").getAsJsonObject()
                .get("ob").getAsJsonObject()
                .get("tempF").getAsString();
    }
    public String getWeather()
    {
        return weatherInfo.getAsJsonObject()
                .get("response").getAsJsonObject()
                .get("ob").getAsJsonObject()
                .get("weather").getAsString();
    }
}

