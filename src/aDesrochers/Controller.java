package aDesrochers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class Controller
{
    @FXML
    TextField zipcodeField;

    @FXML
    TextField cityField;

    @FXML
    TextField stateField;

    @FXML
    TextField tempertureField;

    @FXML
    TextField weatherField;

    public void handleGoButton(ActionEvent e)
    {
        String kw = zipcodeField.getText();
        Weather n = new Weather(kw);
        n.fetch();
        cityField.setText(n.getCity());
        stateField.setText(n.getState());
        tempertureField.setText(n.getTemperture());
        weatherField.setText(n.getWeather());
    }

}


